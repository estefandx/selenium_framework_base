package utilities.manager;


import io.github.bonigarcia.wdm.WebDriverManager;
import utilities.ConfigFileReader;



public class FactoryDriver {


    public  void setDriverBrowser()   {
        ConfigFileReader configFileReader= new ConfigFileReader();
        switch (configFileReader.getBrowser()) {
            case "chrome":
                WebDriverManager.chromedriver().setup();
                break;
            case "firefox":
                WebDriverManager.firefoxdriver().setup();
                break;
            case "edge":
                WebDriverManager.edgedriver().setup();
                break;

            default:
                System.out.println("no se encontro el driver");

        }
    }


}

